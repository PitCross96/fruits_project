import os
from time import time
import json
import hiplot as hip
import webbrowser
from kerastuner import HyperParameters
from tensorflow.python.keras.callbacks import TensorBoard
from fruit_hypermodel import Fruit_hypermodel, Load_fruit_data, zero_one_loss
from kerastuner.tuners import Hyperband
from tensorflow.keras.callbacks import EarlyStopping


img_size = 32
varieties, x_train, y_train, x_test, y_test = Load_fruit_data(img_size)

# tuning network with one convolutional layer
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=1)
hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    max_epochs=50,
    factor=3,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='ft1',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/ft1'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'num_filters_1': vis_data[idx]['hyperparameters']['values']['num_filters_1'],
    'kernel_size_1': vis_data[idx]['hyperparameters']['values']['kernel_size_1'],
    'dropout_1': vis_data[idx]['hyperparameters']['values']['dropout_1'],
    'activation_1': vis_data[idx]['hyperparameters']['values']['activation_1'],
    'units': vis_data[idx]['hyperparameters']['values']['units'],
    'activation_dense': vis_data[idx]['hyperparameters']['values']['activation_dense'],
    'dense_dropout': vis_data[idx]['hyperparameters']['values']['dropout_dense'],
    'learning_rate': vis_data[idx]['hyperparameters']['values']['learning_rate'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_tuning_one.html')
webbrowser.open_new_tab('fruits_tuning_one.html')

best_hp_one = tuner.get_best_hyperparameters()[0]

# tuning network with two convolutional layers
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    max_epochs=50,
    factor=3,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='ft2',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/ft2'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'num_filters_1': vis_data[idx]['hyperparameters']['values']['num_filters_1'],
    'kernel_size_1': vis_data[idx]['hyperparameters']['values']['kernel_size_1'],
    'dropout_1': vis_data[idx]['hyperparameters']['values']['dropout_1'],
    'activation_1': vis_data[idx]['hyperparameters']['values']['activation_1'],
    'num_filters_2': vis_data[idx]['hyperparameters']['values']['num_filters_2'],
    'kernel_size_2': vis_data[idx]['hyperparameters']['values']['kernel_size_2'],
    'dropout_2': vis_data[idx]['hyperparameters']['values']['dropout_2'],
    'activation_2': vis_data[idx]['hyperparameters']['values']['activation_2'],
    'units': vis_data[idx]['hyperparameters']['values']['units'],
    'activation_dense': vis_data[idx]['hyperparameters']['values']['activation_dense'],
    'dense_dropout': vis_data[idx]['hyperparameters']['values']['dropout_dense'],
    'learning_rate': vis_data[idx]['hyperparameters']['values']['learning_rate'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_tuning_two.html')
webbrowser.open_new_tab('fruits_tuning_two.html')

best_hp_two = tuner.get_best_hyperparameters()[0]

# tuning network with three convolutional layers
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=3)
hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    max_epochs=50,
    factor=3,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='ft3',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/ft3'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'num_filters_1': vis_data[idx]['hyperparameters']['values']['num_filters_1'],
    'kernel_size_1': vis_data[idx]['hyperparameters']['values']['kernel_size_1'],
    'dropout_1': vis_data[idx]['hyperparameters']['values']['dropout_1'],
    'activation_1': vis_data[idx]['hyperparameters']['values']['activation_1'],
    'num_filters_2': vis_data[idx]['hyperparameters']['values']['num_filters_2'],
    'kernel_size_2': vis_data[idx]['hyperparameters']['values']['kernel_size_2'],
    'dropout_2': vis_data[idx]['hyperparameters']['values']['dropout_2'],
    'activation_2': vis_data[idx]['hyperparameters']['values']['activation_2'],
    'num_filters_3': vis_data[idx]['hyperparameters']['values']['num_filters_3'],
    'kernel_size_3': vis_data[idx]['hyperparameters']['values']['kernel_size_1'],
    'dropout_3': vis_data[idx]['hyperparameters']['values']['dropout_3'],
    'activation_3': vis_data[idx]['hyperparameters']['values']['activation_3'],
    'units': vis_data[idx]['hyperparameters']['values']['units'],
    'activation_dense': vis_data[idx]['hyperparameters']['values']['activation_dense'],
    'dense_dropout': vis_data[idx]['hyperparameters']['values']['dropout_dense'],
    'learning_rate': vis_data[idx]['hyperparameters']['values']['learning_rate'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_tuning_three.html')
webbrowser.open_new_tab('fruits_tuning_three.html')

best_hp_three = tuner.get_best_hyperparameters()[0]

# training fulltuned networks
best_model_one = hypermodel.build(best_hp_one)
best_model_one.fit(x_train, y_train, epochs=17, callbacks=[TensorBoard(log_dir="logs/fulltuned1_" + str(time()))])
best_model_two = hypermodel.build(best_hp_two)
best_model_two.fit(x_train, y_train, epochs=50, callbacks=[TensorBoard(log_dir="logs/fulltuned2_" + str(time()))])
best_model_three = hypermodel.build(best_hp_three)
best_model_three.fit(x_train, y_train, epochs=50, callbacks=[TensorBoard(log_dir="logs/fulltuned3_" + str(time()))])

print('zero-one loss for tuned model with one convolutional layer: ' + str(zero_one_loss(y_test, best_model_one.predict(x_test))))
print('zero-one loss for tuned model with two convolutional layers: ' + str(zero_one_loss(y_test, best_model_two.predict(x_test))))
print('zero-one loss for tuned model with three convolutional layers: ' + str(zero_one_loss(y_test, best_model_three.predict(x_test))))


# tuning filters number
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('kernel_size_1', value=best_hp_two.get_config()['values']['kernel_size_1'])
hp.Fixed('activation_1', value=best_hp_two.get_config()['values']['activation_1'])
hp.Fixed('dropout_1', value=best_hp_two.get_config()['values']['dropout_1'])
hp.Fixed('kernel_size_2', value=best_hp_two.get_config()['values']['kernel_size_2'])
hp.Fixed('activation_2', value=best_hp_two.get_config()['values']['activation_2'])
hp.Fixed('dropout_2', value=best_hp_two.get_config()['values']['dropout_2'])
hp.Fixed('units', value=best_hp_two.get_config()['values']['units'])
hp.Fixed('activation_dense', value=best_hp_two.get_config()['values']['activation_dense'])
hp.Fixed('dropout_dense', value=best_hp_two.get_config()['values']['dropout_dense'])
hp.Fixed('learning_rate', value=best_hp_two.get_config()['values']['learning_rate'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=2,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='filters',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/filters'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'num_filters_1': vis_data[idx]['hyperparameters']['values']['num_filters_1'],
    'num_filters_2': vis_data[idx]['hyperparameters']['values']['num_filters_2'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_filters.html')
webbrowser.open_new_tab('fruits_filters.html')

best_hp = tuner.get_best_hyperparameters()[0]

# tuning kernel sizes
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('num_filters_1', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('activation_1', value=best_hp.get_config()['values']['activation_1'])
hp.Fixed('dropout_1', value=best_hp.get_config()['values']['dropout_1'])
hp.Fixed('num_filters_2', value=best_hp.get_config()['values']['num_filters_2'])
hp.Fixed('activation_2', value=best_hp.get_config()['values']['activation_2'])
hp.Fixed('dropout_2', value=best_hp.get_config()['values']['dropout_2'])
hp.Fixed('units', value=best_hp.get_config()['values']['units'])
hp.Fixed('activation_dense', value=best_hp.get_config()['values']['activation_dense'])
hp.Fixed('dropout_dense', value=best_hp.get_config()['values']['dropout_dense'])
hp.Fixed('learning_rate', value=best_hp.get_config()['values']['learning_rate'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=2,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='kernel',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/kernel'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'kernel_size_1': vis_data[idx]['hyperparameters']['values']['kernel_size_1'],
    'kernel_size_2': vis_data[idx]['hyperparameters']['values']['kernel_size_2'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_kernel.html')
webbrowser.open_new_tab('fruits_kernel.html')

best_hp = tuner.get_best_hyperparameters()[0]

# tuning activation functions
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('num_filters_1', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_1', value=best_hp.get_config()['values']['kernel_size_1'])
hp.Fixed('dropout_1', value=best_hp.get_config()['values']['dropout_1'])
hp.Fixed('num_filters_2', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_2', value=best_hp.get_config()['values']['kernel_size_2'])
hp.Fixed('dropout_2', value=best_hp.get_config()['values']['dropout_2'])
hp.Fixed('units', value=best_hp.get_config()['values']['units'])
hp.Fixed('dropout_dense', value=best_hp.get_config()['values']['dropout_dense'])
hp.Fixed('learning_rate', value=best_hp.get_config()['values']['learning_rate'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=2,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='activation',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/activation'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'activation_1': vis_data[idx]['hyperparameters']['values']['activation_1'],
    'activation_2': vis_data[idx]['hyperparameters']['values']['activation_2'],
    'activation_dense': vis_data[idx]['hyperparameters']['values']['activation_dense'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_activation.html')
webbrowser.open_new_tab('fruits_activation.html')

best_hp = tuner.get_best_hyperparameters()[0]

# tuning dense layer
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('num_filters_1', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_1', value=best_hp.get_config()['values']['kernel_size_1'])
hp.Fixed('activation_1', value=best_hp.get_config()['values']['activation_1'])
hp.Fixed('dropout_1', value=best_hp.get_config()['values']['dropout_1'])
hp.Fixed('num_filters_2', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_2', value=best_hp.get_config()['values']['kernel_size_2'])
hp.Fixed('activation_2', value=best_hp.get_config()['values']['activation_2'])
hp.Fixed('dropout_2', value=best_hp.get_config()['values']['dropout_2'])
hp.Fixed('activation_dense', value=best_hp.get_config()['values']['activation_dense'])
hp.Fixed('dropout_dense', value=best_hp.get_config()['values']['dropout_dense'])
hp.Fixed('learning_rate', value=best_hp.get_config()['values']['learning_rate'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=2,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='dense',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/dense'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'units': vis_data[idx]['hyperparameters']['values']['units'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_dense.html')
webbrowser.open_new_tab('fruits_dense.html')

best_hp = tuner.get_best_hyperparameters()[0]


# tuning dropout
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('num_filters_1', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_1', value=best_hp.get_config()['values']['kernel_size_1'])
hp.Fixed('activation_1', value=best_hp.get_config()['values']['activation_1'])
hp.Fixed('num_filters_2', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_2', value=best_hp.get_config()['values']['kernel_size_2'])
hp.Fixed('activation_2', value=best_hp.get_config()['values']['activation_2'])
hp.Fixed('units', value=best_hp.get_config()['values']['units'])
hp.Fixed('activation_dense', value=best_hp.get_config()['values']['activation_dense'])
hp.Fixed('learning_rate', value=best_hp.get_config()['values']['learning_rate'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=3,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='dropout',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/dropout'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'dropout_1': vis_data[idx]['hyperparameters']['values']['dropout_1'],
    'dropout_2': vis_data[idx]['hyperparameters']['values']['dropout_2'],
    'dropout_dense': vis_data[idx]['hyperparameters']['values']['dropout_dense'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_dropout.html')
webbrowser.open_new_tab('fruits_dropout.html')

best_hp = tuner.get_best_hyperparameters()[0]

# tuning learning rate
hp = HyperParameters()
hp.Fixed('num_conv_layers', value=2)
hp.Fixed('num_filters_1', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_1', value=best_hp.get_config()['values']['kernel_size_1'])
hp.Fixed('activation_1', value=best_hp.get_config()['values']['activation_1'])
hp.Fixed('dropout_1', value=best_hp.get_config()['values']['dropout_1'])
hp.Fixed('num_filters_2', value=best_hp.get_config()['values']['num_filters_1'])
hp.Fixed('kernel_size_2', value=best_hp.get_config()['values']['kernel_size_2'])
hp.Fixed('activation_2', value=best_hp.get_config()['values']['activation_2'])
hp.Fixed('dropout_2', value=best_hp.get_config()['values']['dropout_2'])
hp.Fixed('units', value=best_hp.get_config()['values']['units'])
hp.Fixed('activation_dense', value=best_hp.get_config()['values']['activation_dense'])
hp.Fixed('dropout_dense', value=best_hp.get_config()['values']['dropout_dense'])

hypermodel = Fruit_hypermodel(input_shape=(img_size, img_size, 3), num_classes=len(varieties))
tuner = Hyperband(
    hypermodel,
    hyperparameters=hp,
    tune_new_entries=True,
    factor=3,
    max_epochs=50,
    objective='val_loss',
    executions_per_trial=1,
    directory='hyperband',
    project_name='learning',
    seed=7
)
#tuner.search_space_summary()

tuner.search(x_train, y_train, validation_split=0.3, callbacks=[EarlyStopping(monitor='val_loss', patience=3)])
#tuner.results_summary()

vis_data = []
rootdir = 'hyperband/learning'
for subdirs, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith("trial.json"):
          with open(subdirs + '/' + file, 'r') as json_file:
            data = json_file.read()
          vis_data.append(json.loads(data))
data = [{
    'learning_rate': vis_data[idx]['hyperparameters']['values']['learning_rate'],
    'loss': vis_data[idx]['metrics']['metrics']['loss']['observations'][0]['value'],
    'val_loss': vis_data[idx]['metrics']['metrics']['val_loss']['observations'][0]['value'],
    'accuracy': vis_data[idx]['metrics']['metrics']['accuracy']['observations'][0]['value'],
    'val_accuracy': vis_data[idx]['metrics']['metrics']['val_accuracy']['observations'][0]['value']
    } for idx in range(len(vis_data))]
hip.Experiment.from_iterable(data).to_html('fruits_learning.html')
webbrowser.open_new_tab('fruits_learning.html')
best_hp = tuner.get_best_hyperparameters()[0]

# training single tuned network
best_model_retuned = hypermodel.build(best_hp)
best_model_retuned.fit(x_train, y_train, epochs=50, callbacks=[TensorBoard(log_dir="logs/retuned_" + str(time()))])
print('zero-one loss for single tuned model : ' + str(zero_one_loss(y_test, best_model_retuned.predict(x_test))))
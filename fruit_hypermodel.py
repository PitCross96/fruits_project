from kerastuner import HyperModel
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import (
    Conv2D,
    Dense,
    Dropout,
    Flatten,
    MaxPooling2D
)
import os
import cv2
import random
import numpy as np
from keras.utils import to_categorical


class Fruit_hypermodel(HyperModel):
    def __init__(self, input_shape, num_classes):
        self.input_shape = input_shape
        self.num_classes = num_classes

    def build(self, hp):
        model = Sequential()
        num_conv_layers = hp.Int('num_conv_layers', min_value=1, max_value=3)
        model.add(
            Conv2D(
                filters=hp.Choice(
                    'num_filters_1',
                    values=[16, 32, 64, 128],
                    default=16
                ),
                kernel_size=hp.Choice(
                    'kernel_size_1',
                    values=[3,5,7],
                    default=3
                ),
                activation=hp.Choice(
                    'activation_1',
                    values=['relu', 'tanh', 'sigmoid'],
                    default='relu'
                ),
                padding='same',
                input_shape=self.input_shape
            )
        )
        model.add(
            MaxPooling2D(
                pool_size=2,
                padding='same'
            )
        )
        model.add(
            Dropout(
                rate=hp.Float(
                    'dropout_1',
                    min_value=0.0,
                    max_value=0.5,
                    default=0,
                    step=0.05
                )
            )
        )
        if num_conv_layers > 1:
            model.add(
                Conv2D(
                    filters=hp.Choice(
                        'num_filters_2',
                        values=[16, 32, 64, 128],
                        default=32
                    ),
                    kernel_size=hp.Choice(
                        'kernel_size_2',
                        values=[3,5,7],
                        default=3
                    ),
                    activation=hp.Choice(
                        'activation_2',
                        values=['relu', 'tanh', 'sigmoid'],
                        default='relu'
                    ),
                    padding='same'
                )
            )
            model.add(
                MaxPooling2D(
                    pool_size=2,
                    padding='same'
                )
            )
            model.add(
                Dropout(
                    rate=hp.Float(
                        'dropout_2',
                        min_value=0.0,
                        max_value=0.5,
                        default=0,
                        step=0.05
                    )
                )
            )
        if num_conv_layers > 2:
            model.add(
                Conv2D(
                    filters=hp.Choice(
                        'num_filters_3',
                        values=[16, 32, 64, 128],
                        default=64
                    ),
                    kernel_size=hp.Choice(
                        'kernel_size_3',
                        values=[3,5,7],
                        default=3
                    ),
                    activation=hp.Choice(
                        'activation_3',
                        values=['relu', 'tanh', 'sigmoid'],
                        default='relu'
                    ),
                    padding='same'
                )
            )
            model.add(
                MaxPooling2D(
                    pool_size=2,
                    padding='same'
                )
            )
            model.add(
                Dropout(
                    rate=hp.Float(
                        'dropout_3',
                        min_value=0.0,
                        max_value=0.5,
                        default=0,
                        step=0.05
                    )
                )
            )

        model.add(Flatten())
        model.add(
            Dense(
                units=hp.Int(
                    'units',
                    min_value=32,
                    max_value=512,
                    step=32,
                    default=256
                ),
                activation=hp.Choice(
                    'activation_dense',
                    values=['relu', 'tanh', 'sigmoid'],
                    default='relu'
                )
            )
        )
        model.add(
            Dropout(
                rate=hp.Float(
                    'dropout_dense',
                    min_value=0.0,
                    max_value=0.5,
                    default=0,
                    step=0.05,
                )
            )
        )
        model.add(Dense(units=self.num_classes, activation='softmax'))

        model.compile(
            optimizer=Adam(
                hp.Float(
                    'learning_rate',
                    min_value=1e-4,
                    max_value=1e-2,
                    sampling='LOG',
                    default=1e-3
                )
            ),
            loss='categorical_crossentropy',
            metrics=['accuracy'],
        )
        return model


def Load_fruit_data(img_size):
    arr1 = []
    arr2 = []
    datadir_train = 'fruits-360/Training/'
    datadir_test = 'fruits-360/Test/'
    categories = ['Apple', 'Banana', 'Plum', 'Pepper', 'Cherry', 'Grape', 'Tomato', 'Potato', 'Pear', 'Peach']

    varieties = []
    for directory in os.listdir(datadir_train):
        for category in categories:
            if category in directory:
                if any(char.isdigit() for char in directory):
                    variety = directory[0:-2]
                else:
                    variety = directory
                if variety not in varieties:
                    varieties.append(variety)

    # For training set
    for directory in os.listdir(datadir_train):
        if any(char.isdigit() for char in directory):
            directory_variety = directory[0:-2]
        else:
            directory_variety = directory
        for variety in varieties:
            if variety == directory_variety:
                path = os.path.join(datadir_train, directory)
                class_num = varieties.index(variety)
                for image in os.listdir(path):
                    if image.endswith('jpg'):
                        img_array = cv2.imread(os.path.join(path, image), cv2.IMREAD_COLOR)
                        img_array = cv2.resize(img_array, (img_size, img_size))
                        arr1.append([img_array, class_num])
    # For test set
    for directory in os.listdir(datadir_test):
        if any(char.isdigit() for char in directory):
            directory_variety = directory[0:-2]
        else:
            directory_variety = directory
        for variety in varieties:
            if variety == directory_variety:
                path = os.path.join(datadir_test, directory)
                class_num = varieties.index(variety)
                for image in os.listdir(path):
                    if image.endswith('jpg'):
                        img_array = cv2.imread(os.path.join(path, image), cv2.IMREAD_COLOR)
                        img_array = cv2.resize(img_array, (img_size, img_size))
                        arr2.append([img_array, class_num])

    x_train = []
    y_train = []
    x_test = []
    y_test = []

    random.shuffle(arr1)
    random.shuffle(arr2)

    # trainig data
    for features, label in arr1:
        x_train.append(features)
        y_train.append(label)
    x_train = np.array(x_train)
    # test data
    for features, label in arr2:
        x_test.append(features)
        y_test.append(label)
    x_test = np.array(x_test)

    #reshaping sets
    x_train = x_train.reshape(-1, img_size, img_size, 3)
    x_train = x_train / 255
    x_test = x_test.reshape(-1, img_size, img_size, 3)
    x_test = x_test / 255
    y_train = to_categorical(y_train, num_classes=len(varieties))
    y_test = to_categorical(y_test, num_classes=len(varieties))

    return varieties, x_train, y_train, x_test, y_test


def zero_one_loss(y_true, y_pred):
    loss = 0
    for i in range(len(y_true)):
        if np.argmax(y_true[i]) != np.argmax(y_pred[i]):
            loss += 1
    return loss/len(y_true)